import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'card-component',
  templateUrl: 'card.component.html'
})
export class CardComponent {
  @Input() public model: any;
  constructor(public navCtrl: NavController) {

  }

}
