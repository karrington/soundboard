import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	public data: any;
	public pages: any;
	public webmedia: any;
	public isPushed = false;

constructor(public navCtrl: NavController) {
    this.data = [
			{ id: 1, title:'Numéro 10', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/bird.mp3', isPlayed: false},
			{ id: 2, title:'Le Bitume avec une plume', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/cat.mp3', isPlayed: false},
			{ id: 3, title:'Pitbull 23', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/cow.mp3', isPlayed: false},
			{ id: 4, title:'92i Veyron', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/dolphin.mp3', isPlayed: false},
			{ id: 5, title:'Le Duc de Boulogne', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/frog.mp3', isPlayed: false},
			{ id: 6, title:'Ma Définition', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/pig.mp3', isPlayed: false},
			{ id: 7, title:'Repose en Paix', icon:'assets/animals/b2o.jpeg', audio:'./assets/sounds/dog.mp3', isPlayed: false},
		];
    this.pages = [this.data, this.data, this.data, this.data];
}


	public play(soundFile, isPushed, id){		
		if(isPushed) {
			isPushed = false;
		} else {
			isPushed = true;
		}
		this.data[id-1].isPlayed = isPushed;
		const onStatusUpdate = (status) => console.log(status);
		if(this.webmedia) {
			this.webmedia.pause();
		}
		this.webmedia = new Audio(soundFile);
		this.webmedia.load();
		this.webmedia.play();
		console.log(this.webmedia.controller);
	}

}